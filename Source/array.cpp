//
//  array.cpp
//  CommandLineTool
//
//  Created by Steffan Davies on 06/10/2016.
//  Copyright © 2016 Tom Mitchell. All rights reserved.
//

#include "array.hpp"


Array::Array(){
    
    noOfItems = 0;
    
};

Array::~Array(){
    
    delete[] floatpoint;
    
};

void Array::add (float itemValue){
    
    //new array, 1 bigger
    float* temp = new float[noOfItems + 1];
    //copy values
    for (int count = 0; count < noOfItems; count++)
    {
        temp[count] = floatpoint[count];
    }
    //add new value to end of array
    temp[noOfItems] = itemValue;
    //Delete old array
    if (floatpoint != nullptr)
    delete[] floatpoint;
    
    
    //point to start of new array
    floatpoint = temp;
    //increment size
    noOfItems++;
};

float Array::get (int index)const {
    
    if (index >= 0 and index < noOfItems)
        return floatpoint[index];
    else
        return 0;
}

int Array::size(){
    
    return noOfItems;
}