//
//  array.hpp
//  CommandLineTool
//
//  Created by Steffan Davies on 06/10/2016.
//  Copyright © 2016 Tom Mitchell. All rights reserved.
//

#ifndef array_hpp
#define array_hpp

#include <stdio.h>

#endif /* array_hpp */

template <class Type>
class Array
{
public:
    
Array(){
    noOfItems = 0;
};
    
~Array(){
    delete[] floatpoint;
};
    
void add (Type itemValue){
    //new array, 1 bigger
    float* temp = new float[noOfItems + 1];
    //copy values
    for (int count = 0; count < noOfItems; count++)
    {
        temp[count] = floatpoint[count];
    }
    //add new value to end of array
    temp[noOfItems] = itemValue;
    //Delete old array
    if (floatpoint != nullptr)
        delete[] floatpoint;
    //point to start of new array
    floatpoint = temp;
    //increment size
    noOfItems++;
};
    
Type get (int index)const {
    
        if (index >= 0 and index < noOfItems)
            return floatpoint[index];
        else
            return 0;
};
    
int size(){
    
        return noOfItems;
};
    
//overload operator: Exercise 1.2.5
bool operator==(Array& otherArray)
{
    if (size() == otherArray.size())
    {
        for(int i = 0; i < noOfItems; i++)
        {
            if (get(i) == otherArray.get(i))
            {
                return true;
            }
        }
    }
    return false;
};
    
private:
    int  noOfItems;
    float *floatpoint = nullptr;

};