//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>
#include "Array.hpp"
#include "Number.h"

bool testArray(); 

int main (int argc, const char* argv[])
{
    testArray();
    
    return 0;
}

bool testArray()
{
    Array<float> array, otherArray;
    const float testArray[] = {0.f, 1.f, 2.f, 3.f, 4.f, 5.f};
    const float testArray2[] = {1.f, 1.f, 2.f, 3.f, 4.f, 5.f};
    const int testArraySize = sizeof (testArray) / sizeof (testArray[0]);
    
    for (int i = 0; i < testArraySize; i++)
    {
        array.add (testArray[i]);
        otherArray.add (testArray2[i]);
    }
    
    if(array == otherArray)
    {
        std::cout << "Array Matches\n";
    }
    else
    {
        std::cout<< "Array does not match\n";
    }
    return true;

    
    
}

